var crypto = require('crypto');
var fs = require('fs');
var path = require('path');
var querystring = require('querystring');

var addr = require('email-addresses');

var conf = {
  domains: [],
  length: 3
};

function parseAddress(local, domain) {
  if (!domain && conf.domains.length === 1) {
    domain = conf.domains[0];
  }

  var address = local;
  if (domain) {
    address += '@' + domain;
  }

  var parsed = addr.parseOneAddress(address);
  if (parsed) {
    return { local: parsed.local, domain: parsed.domain};
  }

  if (domain) {
    return { local: local, domain: domain };
  }

  var at = local.lastIndexOf('@');
  if (at != -1) {
    return { local: local.slice(0, at), domain: local.slice(at + 1)};
  }
}

function md5(str) {
  var hash = crypto.createHash('md5');
  hash.update(str);
  return hash.digest('hex');
}

function checkAddress(local, domain, address) {
  var parsed = addr.parseOneAddress(address);
  return !!parsed && parsed.local === local && parsed.domain === domain;
}

function writeAddress(local, domain) {
  var address = local + '@' + domain;
  if (checkAddress(local, domain, address)) return address;

  var quoted = '"' + local.replace(/\\/g, '\\\\')
                          .replace(/"/g, '\\"') + '"';
  address = quoted + '@' + domain;
  if (checkAddress(local, domain, address)) return address;
}

function hashAddress(local, domain) {
  var parsed = parseAddress(local, domain);
  if (parsed) {
    var key = md5((parsed.local + '@' + parsed.domain).toLowerCase() + '@' + conf.secret).slice(0, conf.length);
    return writeAddress(parsed.local + '-' + key, parsed.domain);
  }
}

/**
 * Set the secret to be used in hash generation
 * @param {string} secret - the secret
 */
exports.setSecret = function(secret) {
  conf.secret = secret;
};

/**
 * List of pre-defined
 * @param {string[]} domains
 */
exports.setDomains = function(domains) {
  conf.domains = domains || [];
};

/**
 * Set the length of hash to use. If this function is not called, the default is `3`
 * @param {int} length - The length. Must be between 1 and 32.
 */
exports.setLength = function(length) {
  conf.length = length;
};

exports.getPage = function(queryString, cb) {
  var query = querystring.parse(queryString);

  var data = {
    domains: conf.domains
  };
  if (query.address_prefix) {
    data.prefix = query.address_prefix;
    data.domain = query.address_domain;
    data.address = hashAddress(data.prefix, data.domain);
    if (!data.address) {
      data.error = 'Cannot parse address';
    }
  }
  fs.readFile(path.join(__dirname, 'index.html'), 'utf8', function(err, index) {
    if (err) {
      return cb(err);
    }
    index = index.replace('__DATA__', JSON.stringify(data));
    cb(null, index);
  });
};
